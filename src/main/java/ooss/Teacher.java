package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private ArrayList<Klass> klasses;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        this.klasses = new ArrayList<>();
    }

    public String introduce() {
        StringBuilder introduce = new StringBuilder();
        introduce.append(super.introduce()).append(" I am a teacher.");
//        if (this.klasses.size() != 0) {
//            String join = " I teach Class ";
//            join += String.join(", ",this.klasses.stream().map(klass -> String.valueOf(klass.getNumber())).collect(Collectors.toList())) + ".";
//            introduce.append(join);
//        }

        if (this.klasses.size() != 0) {
            String join = " I teach Class ";
            join += this.klasses.stream().map(klass -> String.valueOf(klass.getNumber())).collect(Collectors.joining(", ")) + ".";
            introduce.append(join);
        }


//        if (this.klasses.size() != 0) {
//            introduce.append(" I teach Class ");
//            this.klasses.stream().forEach(klass -> introduce.append(klass.getNumber() + ", "));
//            introduce.replace(introduce.length() - 2,introduce.length(),"");
//            introduce.append(".");
//        }
        return introduce.toString();
    }

    public void assignTo(Klass klass) {
        this.klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klasses.contains(klass);
    }

    public boolean isTeaching(Student student){
        Klass klassNum = student.getKlass();
        return this.klasses.contains(klassNum);
    }

}
