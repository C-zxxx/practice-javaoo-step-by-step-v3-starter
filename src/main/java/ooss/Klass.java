package ooss;

import java.util.ArrayList;
import java.util.Objects;

public class Klass {
    private int number;

    private Student leader;

    private ArrayList<Teacher> teachers;

    private ArrayList<Student> students;

    public Klass(int number) {
        this.number = number;
        this.teachers = new ArrayList<Teacher>();
        this.students = new ArrayList<Student>();
    }

    public void assignLeader(Student student) {
        if (student.getKlass() == null || student.getKlass().getNumber() != this.getNumber()) {
            System.out.println("It is not one of us.");
        } else {
            this.leader = student;
            this.teachers.forEach(teacher ->
                    System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.%n", teacher.getName(), this.getNumber(), this.getLeader().getName()));
            this.students.forEach(klassMember ->
                    System.out.printf("I am %s, student of Class %d. I know %s become Leader.%n", klassMember.getName(), this.getNumber(), this.getLeader().getName()));
        }
    }

    public void attach(Person person) {
        if (person instanceof Student) {
            this.students.add((Student) person);
        } else {
            this.teachers.add((Teacher) person);
        }
    }

    public boolean isLeader(Student student) {
        return this.leader.equals(student);
    }

    public Student getLeader() {
        return leader;
    }

    public void setLeader(Student leader) {
        this.leader = leader;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ArrayList<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(ArrayList<Teacher> teachers) {
        this.teachers = teachers;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
