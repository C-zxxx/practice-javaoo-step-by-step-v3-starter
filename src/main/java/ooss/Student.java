package ooss;

import java.util.ArrayList;

public class Student extends Person {

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce() {
        StringBuilder introduce = new StringBuilder();
        introduce.append(super.introduce()).append(" I am a student.");

        if (this.klass != null) {
            if (this.klass.getLeader() == null || this.klass.getLeader().getId() != getId()) {
                introduce.append(" I am in class ").append(klass.getNumber()).append(".");
            }
            if (this.klass.getLeader() != null && this.klass.getLeader().getId() == this.getId()) {
                introduce.append(" I am the leader of class ").append(klass.getNumber()).append(".");
            }
        }

        return introduce.toString();
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return klass.equals(this.klass);
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }

}
